<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <meta charset="UTF-8">
    <title>Todo List</title>
    <style>
    body{
      background-image: url("images/coneil3.jpg");
      background-repeat: no-repeat;
      background-size: cover;
    }

    h2{
    text-align: center;

    }

    p{
    text-align: center;
    font-color: black;
    }
    </style>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/nav-bar.jsp"/>
<div class="container-fluid">
    <div class="row mt-4">
        <div class="col-12">
            <h2>Todo Items</h2>
        </div>
    </div>
<table class="table table-striped">
    <thead>
    <tr>
        <td><label>Title</label></td>
        <td><label>Completed</label></td>
        <td><label>Action</label></td>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="todo" items="${todos}">
    <tr>
        <td>
            ${todo.title}
        </td>
        <td>
            ${todo.completed}
        </td>
        <td>
            <a href="${pageContext.request.contextPath}/todo/edit/${todo.id}">Edit</a>
            &nbsp;
            <a href="${pageContext.request.contextPath}/todo/complete/${todo.id}">Complete</a>
            &nbsp;
            <a href="${pageContext.request.contextPath}/todo/delete/${todo.id}">Delete</a>
        </td>
    </tr>
    </c:forEach>
</table>
</div>
<p>You successfully added data in on the: <%= (new java.util.Date()).toLocaleString()%></p>
</body>
</html>
