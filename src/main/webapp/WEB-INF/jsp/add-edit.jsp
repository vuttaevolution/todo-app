<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <link rel="stylesheet" text/style ="css" href="/css/style.css">
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <meta charset="UTF-8">
        <title>Todo List</title>

</head>
<body>
<jsp:include page="/WEB-INF/jsp/nav-bar.jsp"/>
<h2>${modeTitle} Todo</h2>
<script src="/js/validation.js"></script>
<div class="container col-9">
    <form:form action="${pageContext.request.contextPath}/todo/${mode}" method="post" modelAttribute="todo">
        <form:hidden path="id"/>
        <div class="form-group offset-1 col-10">
            <form:label path="title">Title</form:label>
            <div class="row justify-content-start">
                <form:input class="form-control" path="title"></form:input>
            </div>
            <div class="row justify-content-end">
                <form:errors class="alert alert-danger " role="alert" path="title"></form:errors>
            </div>
        </div>
        <div class="form-group offset-1 col-10">
            <form:label path="description">Description</form:label>
            <div class="row justify-content-start">
                <form:textarea class="form-control" path="description" cols="60" rows="10"></form:textarea>
            </div>
            <div class="row justify-content-end">
                <form:errors class="alert alert-danger" role="alert" path="description"></form:errors>
            </div>
        </div>
        <div class="row offset-1 col-10">
            <input class="btn btn-primary" type="submit" name="submit" value="Submit">
            <form:button class="btn btn-secondary" name="cancel">Cancel</form:button>
        </div>
    </form:form>
</div>
</body>
</html>
